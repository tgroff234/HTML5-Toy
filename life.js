var cells = Array();

var grid_size = 3;
var zoom_min = 0;
var zoom_max = 40;
var zoom_level = 0;


var zoom_center = [50,50];

var life_chance = 0.35;
var framerate = 10;

$(document).ready(function(){

    // Vars
    var cnvs = $("#canvas")[0];
    var c = cnvs.getContext("2d");

    // Functions
    var seed = function(size_x, size_y){
      console.log("X: "+size_x+", Y: "+size_y);
      for(y=0; y<size_y; y++){
        cells[y] = Array()
        for(x=0; x<size_x; x++){
            cells[y][x] = (Math.random() < life_chance);
        }
      }
    }

    var calc = function(board, size_x, size_y){

      var result = new Array(Math.max(size_y,board.length));
      for(i=0;i<Math.max(size_y,board.length);i++){
        result[i] = new Array(Math.max(size_x, board[0].length));
      }

      for(y=0; y<size_y;y++){
      for(x=0; x<size_x; x++){
        if(typeof board[y] == "undefined" || typeof board[y][x] == "undefined"){
          result[y][x] = (Math.random() < life_chance);
        }
      }
      }
      for(y=0;y<board.length;y++){
      for(x=0;x<board[y].length;x++){
        var n = 0;

        // Neighbor Check
        for(dx=-1;dx<=1;dx++){
        for(dy=-1;dy<=1;dy++){
          if ( dx == 0 && dy == 0){}
					else if (typeof board[y+dy] != 'undefined'
						       && typeof board[y+dy][x+dx] != 'undefined'
						       && board[y+dy][x+dx]){
						n++;
					}
        }
        }
        var state = board[y][x];
				switch (n) {
					case 0:
					case 1:
						state = false;
						break;
					case 2:
						break;
					case 3:
						state = true;
						break;
					default:
						state = false;
				}
			  result[y][x] = state;
      }
      }
      return result
    }

    // Init
    var width = $("#canvas").width();
    var height = $("#canvas").height();

    cnvs.width = width;
    cnvs.height = height;

    var margin_x = (width%grid_size)/2;
    var margin_y = (height%grid_size)/2;

    var grid_x = (width-margin_x*2)/grid_size;
    var grid_y = (height-margin_y*2)/grid_size;

    var board_size = [(width-margin_x*2)/grid_size,(height-margin_y*2)/grid_size];

    seed(board_size[0], board_size[1]);
    cells = calc(cells, board_size[0], board_size[1]);


    $(window).mousewheel(function(turn, delta){
      if(delta > 0 && zoom_level < zoom_max){
        zoom_level++;
        c.scale(1.1,1.1);
        c.translate(-(turn.pageX*0.1), -(turn.pageY*0.1));
      }else if(delta < 0 && zoom_level > 0){
        zoom_level--;
        c.scale(0.9,0.9);
        c.translate((turn.pageX*0.1), (turn.pageY*0.1));
      }
      console.log(zoom_level);
      //if(delta > 0){
      //  grid_size = grid_size > zoom_max ? zoom_max : grid_size+1;
      //  console.log("zoom in");
      //}else{
      //  grid_size = grid_size <= zoom_min+1 ? zoom_min : grid_size-1;
      //  console.log("zoom out");
      //}

      //console.log("X: "+zoom_center[0]+", Y: "+zoom_center[1]);
    });

    // Main Loop
    window.setInterval(function(){

      width = $("#canvas").width();
      height = $("#canvas").height();

      margin_x = (width%grid_size)/2;
      margin_y = (height%grid_size)/2;

      grid_x = (width-margin_x*2)/grid_size;
      grid_y = (height-margin_y*2)/grid_size;

      //console.log("X: "+grid_x+", Y: "+grid_y);

      //cnvs.width = width;
      //cnvs.height = height;

      c.fillStyle = "#000033";
      c.fillRect(0,0,width, height);

      cells = calc(cells, grid_x, grid_y);

      for(y=0; y<grid_y; y++){
        for(x=0; x<grid_x; x++){

          var offset_x = grid_size*x;
          var offset_y = grid_size*y;

          var row,column;

          if(zoom_center[0] < Math.floor(grid_x/2)){
            column = 0+x;
          }else if(zoom_center[0] > board_size[0]-Math.floor(grid_x/2)){
            column = board_size[0]-grid_x+x;
          }else{
            column = zoom_center[0]-Math.floor(grid_x/2)+x;
          }
          if(zoom_center[1] < Math.floor(grid_y/2)){
            row = 0+y;
          }else if(zoom_center[1] > board_size[1]-Math.floor(grid_y/2)){
            row = board_size[1]-grid_y+y;
          }else{
            row = zoom_center[1]-Math.floor(grid_y/2)+y;
          }

          c.fillStyle = "#000066";

          if(cells[row][column]){
            c.fillRect(1+offset_x, offset_y+1, grid_size-1, grid_size-1);

          }

        }
      }

      c.fillStyle = "yellow";
      c.fillRect(1+grid_size*zoom_center[0], 1+grid_size*zoom_center[1], grid_size-1, grid_size-1);

      c.fill();

    }, 1000/framerate);

});
