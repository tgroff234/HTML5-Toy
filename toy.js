$(document).ready(function(){
    var pressed={};

    var toVec = function(x, y){
      return [Math.sqrt(x*x+y*y), Math.atan2(y,x)];
    }

    var toCartesian = function(vector){
      var x = Math.cos(vector[1])*vector[0];
      var y = Math.sin(vector[1])*vector[0];
      return [x,y];
    }

    var toRad = function(degrees){
      return (degrees/360)*(2*Math.PI);
    }
    var toDeg = function(radians){
      return (radians/(2*Math.PI))*360;
    }

    var addVec = function(a, b){
      var c = toCartesian(a);
      var d = toCartesian(b);

      var x = c[0]+d[0];
      var y = c[1]+d[1];

      return toVec(x,y);
    }

    var cnvs = $("#canvas")[0];
    var c = cnvs.getContext("2d");
    var run = true;

    var prev = {};
    var curr = {};

    var vel = [0, 0];

    var width = $("#canvas").width();
    var height = $("#canvas").height();

    cnvs.height = height;
    cnvs.width = width;

    //console.log(width);
    //console.log(height);
    var x, y, size;
    x = width/2;
    y = height/2;
    size = 25;

    var mouseX, mouseY;

    // Physics Vars
    var en_gravity = false;
    var g = [0.15, toRad(-90)];
    var bounce = 0.85;
    var key_strength = 0.2;
    var mag_radius = 500;
    var mag_strength = 20;



    $(window).keydown(function(e){
      e = e || window.event;
      pressed[e.keyCode] = true;
    });

    $(window).keyup(function(e){
      e = e || window.event;
      delete pressed[e.keyCode];
    });

    $(window).mousedown(function(e){
      switch(e.button){
        case 0:
          pressed["lmb"] = true;
          break;
        case 1:
          pressed["mmb"] = true;
          break;
        case 2:
          pressed["rmb"] = true;
          break;
      }
    });
    $(window).mouseup(function(e){
      switch(e.button){
        case 0:
          delete pressed["lmb"];
          break;
        case 1:
          delete pressed["mmb"];
          break;
        case 2:
          delete pressed["rmb"];
          break;
      }
    });
    $(window).mousemove(function(e){
      mouseX = e.pageX;
      mouseY = e.pageY;
    });

    window.setInterval(function(){
      width = $("#canvas").width();
      height = $("#canvas").height();

      //console.log("vel: " + vel[0]+", direction: " + toDeg(vel[1]))

      cnvs.height = height;
      cnvs.width = width;

      prev = JSON.parse(JSON.stringify(curr));;
      curr = JSON.parse(JSON.stringify(pressed));


      if(!prev[32] && curr[32]){
        en_gravity = !en_gravity;
      }
      if(!prev[27] && curr[27]){
        //console.log("pause");
        run = !run;
      }

      if(run){

        // Physics
        if(en_gravity){
          vel = addVec(vel, g);
        }
        var v_c = toCartesian(vel);
        x = x+v_c[0];
        y = y-v_c[1];

        if(x < size || x > width-size){
          vel[1] = Math.PI-vel[1];
          x = x<size ? size:width-size;
          vel[0] *= bounce;
        }
        if(y < size || y > height-size){
          vel[1] *= -1;
          y = y<size ? size:height-size;
          vel[0] *= bounce;
        }

        if(pressed[38]){
          vel = addVec(vel, [key_strength, toRad(90)]);
        }
        if(pressed[40]){
          vel = addVec(vel, [key_strength, toRad(-90)]);
        }
        if(pressed[37]){
          vel = addVec(vel, [key_strength, toRad(180)]);
        }
        if(pressed[39]){
          vel = addVec(vel, [key_strength, toRad(0)]);
        }
      }

      c.clearRect(0,0, width, height);
      c.beginPath();

      if(pressed["lmb"]){
        toMouse = toVec(mouseX-x, y-mouseY);
        console.log(toMouse[0]);
        toMouse[0] = mag_strength/toMouse[0];

        vel = addVec(vel, toMouse);

        c.strokeStyle = "red";
        c.lineWidth=10;
        c.globalAlpha = toMouse[0]*4;
        c.moveTo(x,y);
        c.lineTo(mouseX,mouseY);
        c.stroke();
        c.globalAlpha = 1;


      }
      c.beginPath();
      c.strokeStyle = "black";
      c.fillStyle = "#333333";
      c.ellipse(x, y, size, size,0,0,360,false);
      c.fill();



    }, 1000/60);
});
